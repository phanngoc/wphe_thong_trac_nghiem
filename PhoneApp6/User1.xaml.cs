﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Media;
namespace PhoneApp6
{
    public partial class User1 : UserControl
    {
        public String userchoice="";
        public Question ques;
        public String layoutname;
        User2 controlcomment;
        public User1()
        {
            Helper.write("khoi dong user1");
            InitializeComponent();
            int screenHeight = (int) System.Windows.Application.Current.Host.Content.ActualHeight;
            question.Height = screenHeight / 3;
           // Zoompopup.IsOpen = false;
        }
        public void setQuestion(Question ques)
        {
            Helper.write(" "+ques.idquestion);
            this.ques = ques;
          //  question.Source =  new BitmapImage(new Uri(ques.question, UriKind.RelativeOrAbsolute));
        
            string html = 
                @"<html>
                  <head>
                <meta name='viewport' content='width=320,user-scalable=yes' />
                  </head>
                  <body>
                    <img src="+ques.question+@" />
                  </body>
                 </html>
                ";

            question.NavigateToString(html);
            //question.Source = new Uri(ques.question, UriKind.RelativeOrAbsolute);
           
            A.Content = "A";
            B.Content = "B";
            C.Content = "C";
            D.Content = "D";

            A.GroupName = "Group" + this.ques.idquestion;
            B.GroupName = "Group" + this.ques.idquestion;
            C.GroupName = "Group" + this.ques.idquestion;
            D.GroupName = "Group" + this.ques.idquestion;
           

            LayoutRoot.Name = "Test" + this.ques.idquestion;
            layoutname = "Test" + this.ques.idquestion;
           
        }
        public void reset()
        {
            A.IsChecked = false;
            B.IsChecked = false;
            C.IsChecked = false;
            D.IsChecked = false;
            A.Background = new SolidColorBrush(Colors.Transparent);
            B.Background = new SolidColorBrush(Colors.Transparent);
            C.Background = new SolidColorBrush(Colors.Transparent);
            D.Background = new SolidColorBrush(Colors.Transparent);
 
            object comment = FrameworkElementExtensions.FindDescendantByName(this, "Layoutcomment");
            if (comment != null)
            {
                panelscroll.Children.Remove(controlcomment);
            }
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            // Add code to perform some action here.
            System.Diagnostics.Debug.WriteLine("RadioButton_Checked");
            string name = (sender as RadioButton).Name;
            userchoice = name;
        }

        private void Usersubmit(object sender, RoutedEventArgs e)
        {
            if(userchoice==this.ques.correct)
            {
                MessageBox.Show("Bạn đã làm đúng");
                showComment();
            }
            else if(userchoice=="")
            {
                MessageBox.Show("Bạn chưa chọn đáp án");
            }
            else
            {
                MessageBox.Show("Đáp án đúng là "+this.ques.correct);
                object comment = FrameworkElementExtensions.FindDescendantByName(this,this.ques.correct);
                (comment as RadioButton).Background = new SolidColorBrush(Color.FromArgb(200, 162, 193, 57));
                showComment();
            }
        }

        private void Zoom_Click(object sender, RoutedEventArgs e)
        {
            Helper.write("zoom");
           /* Zoompopup.IsOpen = true;
            ImageLarge.Source = new BitmapImage(new Uri(ques.question, UriKind.RelativeOrAbsolute));
            RotateTransform rt = new RotateTransform();
            rt.Angle = 90;
            ImageLarge.RenderTransform = rt;
            int screenWidth = (int)System.Windows.Application.Current.Host.Content.ActualWidth;
            int screenHeight = (int) System.Windows.Application.Current.Host.Content.ActualHeight;
            Helper.write("w:" + screenWidth + "|" + screenHeight);
            ImageLarge.Width = screenWidth;
            ImageLarge.Height = screenHeight;
            */
            
           /* RotateTransform rt = new RotateTransform();
            rt.Angle = 90;
            question.RenderTransform = rt;
            int screenWidth = (int)System.Windows.Application.Current.Host.Content.ActualWidth;
            int screenHeight = (int) System.Windows.Application.Current.Host.Content.ActualHeight;
            */
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content as MainPage;
            currentPage.showPopup(ques.question);
            //question.Height = screenHeight;
            //question.Source = new BitmapImage(new Uri(ques.question, UriKind.RelativeOrAbsolute));
        }
        private void Next_Click(object sender, RoutedEventArgs e)
        {
            Helper.write("next");
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content as MainPage;
            currentPage.nextTest();
        }
        private void showComment()
        {
            controlcomment = new User2();
            controlcomment.setListComment(this.ques.comments,this);
            Grid root = (this.FindName(layoutname) as Grid);
            Helper.write("check da chua comment chua " + this.ques.idquestion);
            object comment = FrameworkElementExtensions.FindDescendantByName(this, "Layoutcomment");
            if (comment == null)
            {
                Helper.write("ko chua comment " + this.ques.idquestion);
                //Grid.SetRow(controlcomment, 3);
                panelscroll.Children.Add(controlcomment);
            }
            
        }
        public void restartComment()
        {
                panelscroll.Children.Remove(controlcomment);
                panelscroll.Children.Add(controlcomment);
        }

    }
}
