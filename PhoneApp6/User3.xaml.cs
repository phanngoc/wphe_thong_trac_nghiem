﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
namespace PhoneApp6
{
    public partial class User3 : UserControl
    {
        private ItemG item;
        public User3()
        {
            InitializeComponent();

        }
        public void setItem(ItemG itemg)
        {
            this.item = itemg;
            String link;
            (GridA.Children[1] as TextBlock).Text = item.item1.name;
            (GridB.Children[1] as TextBlock).Text = (item.item2 == null) ? "" : item.item2.name;
            link = (item.item2 == null) ? "" : "Assets/Images/itemgrid.jpg";
            (GridB.Children[0] as Image).Source = new BitmapImage(new Uri(link,UriKind.RelativeOrAbsolute));
            (GridC.Children[1] as TextBlock).Text = (item.item3 == null) ? "" : item.item3.name;
            link = (item.item3 == null) ? "" : "Assets/Images/itemgrid.jpg";
            (GridC.Children[0] as Image).Source = new BitmapImage(new Uri(link, UriKind.RelativeOrAbsolute));
            (GridD.Children[1] as TextBlock).Text = (item.item4 == null) ? "" : item.item4.name;
            link = (item.item4 == null) ? "" : "Assets/Images/itemgrid.jpg";
            (GridD.Children[0] as Image).Source = new BitmapImage(new Uri(link, UriKind.RelativeOrAbsolute));
            
        }
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            Grid getgrid = (sender as Grid);
            Helper.write("chinh sua grid:" + getgrid.Height + "|" + getgrid.Width);
            getgrid.Height = getgrid.ActualWidth;

        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            Image getimg = (sender as Image);
            Helper.write("chinh sua image:" + getimg.Height + "|" + getimg.Width);
            getimg.Height = getimg.ActualWidth;
        }

        private void GridA_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Helper.write("tap");
            if(item.item1!=null)
            {
                PhoneApplicationService.Current.State["item"] = item.item1;
                Uri uri = (new Uri("/Page3.xaml", UriKind.RelativeOrAbsolute));
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri); 
            }
        }

        private void GridB_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Helper.write("tap");
            if (item.item2 != null)
            {
                PhoneApplicationService.Current.State["item"] = item.item2;
                Uri uri = (new Uri("/Page3.xaml", UriKind.RelativeOrAbsolute));
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri); 
            }
        }

        private void GridC_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Helper.write("tap");
            if (item.item3 != null)
            {
                PhoneApplicationService.Current.State["item"] = item.item3;
                Uri uri = (new Uri("/Page3.xaml", UriKind.RelativeOrAbsolute));
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri); 
            }
        }
        private void GridD_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Helper.write("tap");
            if (item.item4 != null)
            {
                PhoneApplicationService.Current.State["item"] = item.item4;
                Uri uri = (new Uri("/Page3.xaml", UriKind.RelativeOrAbsolute));
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri); 
            }
        }
    }
}
