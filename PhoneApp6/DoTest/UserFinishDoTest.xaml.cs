﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PhoneApp6.DoTest
{
    public partial class UserFinishDoTest : UserControl
    {

        public UserFinishDoTest()
        {
            InitializeComponent();
        }
        public void showResult(String clock,int countCorrectSente,int totalSentence)
        {
            notComplete.Visibility = Visibility.Collapsed;
            Complete.Visibility = Visibility.Visible;
            countCorrect.Text = Convert.ToString(countCorrectSente) + " / " + Convert.ToString(totalSentence);
            time.Text = clock;
        }
        public void showNotComplete()
        {
            notComplete.Visibility = Visibility.Visible;
            Complete.Visibility = Visibility.Collapsed;
        }
        private void Watch_Answer(object sender,RoutedEventArgs e)
        {
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content as MainPage;
            currentPage.showAllAnswerAndComment();
            currentPage.nextTest();
        }
        private void DoNewTest(object sender, RoutedEventArgs e)
        {
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content as MainPage;
            currentPage.backToTest();
        }
        private void DoAgain(object sender, RoutedEventArgs e)
        {
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content as MainPage;
            currentPage.resetTest();
        }
        
    }
}
