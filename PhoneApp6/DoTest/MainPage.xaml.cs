﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Threading;
using System.IO.IsolatedStorage;
namespace PhoneApp6.DoTest
{
    public partial class MainPage : PhoneApplicationPage
    {
        private DispatcherTimer dispatcherTimer;
        int countCorrentAnswer = 0;
        int countChooseAnswer = 0;
        int second = 0;
        int minute = 0;
        int hour = 0;
        String showClock="";
        public int idtest;
        public Test test;
        bool flag_init = false;
        bool flag_update_point = false;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            Sample smple = new Sample();
            smple.ImageSource = new Uri("Assets/Images/712.GIF", UriKind.Relative);
            this.DataContext = smple;
            
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1); // 1 Seconds
            dispatcherTimer.Tick += new EventHandler(dt_Tick);
            dispatcherTimer.Start();
         
        }

        void dt_Tick(object sender, EventArgs e)
        {
            second++;
            if (second == 60) { second = 0; minute++; }
            if (minute == 60) { hour++; minute = 0; }
            setLabelClock();
        }
        void setLabelClock()
        {
            String text_second;
            String text_minute;
            String text_hour;

            text_second = (second < 10) ? "0" + Convert.ToString(second) : Convert.ToString(second);
            text_minute = (minute < 10) ? "0" + Convert.ToString(minute) : Convert.ToString(minute);
            text_hour = (hour < 10) ? "0" + Convert.ToString(hour) : Convert.ToString(hour);
            showClock = text_hour + ":" + text_minute + ":" + text_second;
            clock.Text = showClock;
        }
        public void resetTest()
        {
            second = 0;
            minute = 0;
            hour = 0;
            countCorrentAnswer = 0;
            countChooseAnswer = 0;
            dispatcherTimer.Start();
            for (int i = 0; i < Test.Items.Count - 1; i++)
            {
                ((Test.Items[i] as PivotItem).Content as UserQuestion).reset();
            }
            this.nextTest();

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            //idparent = (NavigationContext.QueryString["id"] as Category).id;
            var k = PhoneApplicationService.Current.State["item"];
            test = k as Test;
            idtest = (k as Test).id;

            init();
        }
        public void init()
        {
            Random rnd = new Random();
            int rand = rnd.Next(1, 1000); // creates a number between 1 and 12
            string avatarUri = "http://phanngoc.uni.me/tracnghiem/index.php/blog/test/" + idtest + "?rand=" + rand;

            HttpWebRequest WebReq =
              (HttpWebRequest)WebRequest.Create(avatarUri);
            WebReq.Method = "GET";
            WebReq.BeginGetResponse(new AsyncCallback(getWord), WebReq);
        }
        private void getWord(IAsyncResult result)
        {
            HttpWebRequest webRequest = (HttpWebRequest)result.AsyncState;
            var httpResponse = (HttpWebResponse)webRequest.EndGetResponse(result);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string responseText = streamReader.ReadToEnd();
                processJson(responseText);
                streamReader.Close();
            }
        }
        private void processJson(string result)
        {
            result = Regex.Replace(result, "<!--.*?-->", "", RegexOptions.Singleline);
            result = Regex.Replace(result, "<script.*?script>", "", RegexOptions.Singleline);
            Helper.write("result:" + result);
            JArray jAr = JArray.Parse(result);
            List<Question> temp;
            temp = new List<Question>();
            for (int i = 0; i < jAr.Count; i++)
            {
                JArray comments = (JArray)jAr[i]["COMMENT"];
                List<Comment> com = new List<Comment>();
                for (int j = 0; j < comments.Count;j++ )
                {
                    JObject userinfo= (JObject)comments[j]["USERINFO"][0];
                    User user = new User((int)userinfo["ID"], (String)userinfo["USERNAME"],(String) userinfo["PASSWORD"], (String)userinfo["FULLNAME"], "http://phanngoc.uni.me/tracnghiem/public/data/avatar/"+(String)userinfo["AVATAR"]);
                    com.Add(new Comment((int)comments[j]["ID"], (String)comments[j]["TEXT"], user, (int)comments[j]["SENTENCEID"]));
                }

                Question ques = new Question((int)jAr[i]["ID"],"http://phanngoc.uni.me/tracnghiem/public/data/question/"+(String)jAr[i]["QUESTION"], (String)jAr[i]["CORRECT"], com,test);
                temp.Add(ques);
            }
            UIThread.Invoke(() => { createLayout(temp); ImageLoading.Visibility = Visibility.Collapsed; });
        }
        private void createLayout(List<Question> temp)
        {
            for (int i = 0; i < temp.Count; i++)
            {
                PivotItem item = new PivotItem();
                UserQuestion user = new UserQuestion();
                user.setQuestion(temp[i]);
                item.Content = user;
                Test.Items.Add(item);
            }
            PivotItem finish = new PivotItem();
            UserFinishDoTest user6 = new UserFinishDoTest();

            finish.Content = user6;
            Test.Items.Add(finish);
            
        }
        public void showAllAnswerAndComment()
        {
            for(int i=0;i<Test.Items.Count-1;i++)
            {
                ((Test.Items[i] as PivotItem).Content as UserQuestion).showComment();
            }
        }
        public void backToTest()
        {
            NavigationService.RemoveBackEntry();
            NavigationService.GoBack();
            //NavigationService.RemoveBackEntry();
        }

        public void showPopup(String text)
        {
            Helper.write(text);
            Zoompopup.IsOpen = true;
           /* BitmapImage bit = new BitmapImage(new Uri(text, UriKind.RelativeOrAbsolute));
            ImageLarge.Source = bit;

            //((RotateTransform)ImageLarge.RenderTransform).Angle = 90;
            //((CompositeTransform)ImageLarge.RenderTransform).Rotation += 90;
           // RotateTransform rt = new RotateTransform();
            //rt.Angle = 90;
            //ImageLarge.RenderTransform = rt;
            int screenWidth = (int)System.Windows.Application.Current.Host.Content.ActualWidth;
            int screenHeight = (int)System.Windows.Application.Current.Host.Content.ActualHeight;
            Helper.write("w:" + screenWidth + "|" + screenHeight);
            ImageLarge.Width = screenWidth;
            ImageLarge.Height = screenHeight;
            */
            
                       string html =
                        @"
                          <!DOCTYPE html>  
                          <html>
                          <head>
                            <meta name='viewport' content='width=200,user-scalable=no' />
                            <style>
                                    body
                                    {
                                        min-height:800px;
                                        min-width:480px;    
                                    }    
                                    img{
                                        -ms-transform: rotate(90deg);
                                        transform: rotate(90deg);
                                        border:1px solid #1E05FF;
                                        display:none;
                                    }
                            </style>
                           
                          </head>
                          <body>
                            <img src=" + text + @" id='myimage'/>
                            <canvas id='myCanvas' width='300' height='800' style='border:1px solid #d3d3d3;'>
                            Your browser does not support the HTML5 canvas tag.</canvas>
                             <script>

                                var c = document.getElementById('myCanvas');
                                var ctx = c.getContext('2d');
                                var img = document.getElementById('myimage');

                                ctx.translate(c.width, 0);
                                ctx.rotate(90*Math.PI/180);
                                ctx.drawImage(img,0,0);

                            </script>
                          </body>
                         </html>
                        ";
            
                       questionzoom.NavigateToString(html);
        }
        private void Test_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(!flag_init)
            {
                flag_init = true;
                return;
            }
            
            if ((Test.SelectedIndex + 1) == Test.Items.Count)
            {
                if (countChooseAnswer!=(Test.Items.Count-1))
                {
                    ((Test.Items[Test.SelectedIndex] as PivotItem).Content as UserFinishDoTest).showNotComplete();
                    
                }
                else
                {
                    if(!flag_update_point)
                    {
                        flag_update_point = true;
                        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                        User user;
                        settings.TryGetValue<User>("usercurrent", out user);
                        float point = (float)(countCorrentAnswer) / (float)(Test.Items.Count - 1) * 10;

                        WebClient client = new WebClient();
                        client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(client_DownloadStringCompleted);
                        string urltoppoint = "http://phanngoc.uni.me/tracnghiem/index.php/blog/updateUserHaveDoneTest/" + idtest + "/" + user.id + "/" + point;
                        client.DownloadStringAsync(new Uri(urltoppoint));
                    }
                  


                    dispatcherTimer.Stop();
                    ((Test.Items[Test.SelectedIndex] as PivotItem).Content as UserFinishDoTest).showResult(showClock, countCorrentAnswer, Test.Items.Count - 1);
                }
            }
            
        }
        void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
           
        }
        public void increaseCorrectAnswer()
        {
            countCorrentAnswer++;
        }
        public void increaseChooseAnswer()
        {
            countChooseAnswer++;
        }
        public void nextTest()
        {
            if((Test.SelectedIndex+1)<Test.Items.Count)
            {
                Test.SelectedIndex = Test.SelectedIndex + 1;
            }
            else
            {
                Test.SelectedIndex = 0;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Zoompopup.IsOpen = false;
        }
    }
}