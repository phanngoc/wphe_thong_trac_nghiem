﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text;
using System.IO;
using Microsoft.Phone.Tasks;
using System.Threading.Tasks;

using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;


namespace PhoneApp6
{
    public partial class User2 : UserControl
    {
        private List<Comment> listc;
        private User user;
        private User1 ques;
        String post_text;
        public User2()
        {
            InitializeComponent();
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            // txtInput is a TextBox defined in XAML.
            
            settings.TryGetValue<User>("usercurrent", out user);
            DataContext = user;
            //load();
        }
        public async void load()
        {
            this.user = await IsolatedStorageOperations.Load<User>("myXML.xml");
            Helper.write(user.avatar);
        }
        public void setListComment(List<Comment> listc,User1 ques)
        {
            this.ques = ques;
            this.listc = listc;
            Helper.write("listc:" + listc.Count);
            if(listc.Count==0)
            {
                User user = new User(100, "Admin", "sdsd", "defeg", "Assets/Images/anony.jpg");
                Comment commen = new Comment(100,"Hãy bình luận những thắc mắc của bạn tại đây", user,3);
                listc.Add(commen);
            }
            //object comment = FrameworkElementExtensions.FindDescendantByName(this, "listcomment");
            Helper.write("comment:" + comment);
            listcomment.ItemsSource = listc;
        }
        private void EditImage(object sender, RoutedEventArgs e)
        {
            //start from here to set width and height.
            //you can do it directly, or by the Binding
            
            Image getimg = (sender as Image);
            Helper.write("chinh sua image:" + getimg.Height + "|" + getimg.Width);
            getimg.Height = getimg.ActualWidth;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            post_text = "TEXT=" + textcomment.Text + "&USERID=" + user.id+"&SENTENCEID="+ques.ques.idquestion;

            var request = HttpWebRequest.Create("http://phanngoc.uni.me/tracnghiem/index.php/blog/post_comment") as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);
        }
        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
            // End the stream request operation
            Stream postStream = request.EndGetRequestStream(asynchronousResult);

            // avoid bad-touching UI stuff
            byte[] byteArray = Encoding.UTF8.GetBytes(post_text);

            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();

            //Start the web request
            request.BeginGetResponse(new AsyncCallback(GetResponceStreamCallback), request);

        }
        void GetResponceStreamCallback(IAsyncResult callbackResult)
        {
            HttpWebRequest request = (HttpWebRequest)callbackResult.AsyncState;
            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(callbackResult);
            using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
            {
                string result = httpWebStreamReader.ReadToEnd();
                //For debug: show results
                result = Regex.Replace(result, "<!--.*?-->", "", RegexOptions.Singleline);
                result = Regex.Replace(result, "<script.*?script>", "", RegexOptions.Singleline);
                Helper.write(result);
                JArray jAr = JArray.Parse(result);
                
                Comment commen = new Comment((int)jAr[0]["ID"],(String)jAr[0]["TEXT"],user,(int)jAr[0]["SENTENCEID"]);
                listc.Add(commen);
                UIThread.Invoke(() =>
                {
                    listcomment.ItemsSource = listc;
                    ques.restartComment();
                });
                
            }
        }

    }
}
