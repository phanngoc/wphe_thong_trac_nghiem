﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp6
{
    public class Comment
    {
        public User user {set;get;}
        public String text { set; get; }

        public int id { set; get; }
        public int sentenceid { set; get; }
        public Comment(int id,String text,User user,int sentenceid)
        {
            this.id = id;
            this.user = user;
            this.text = text;
            this.sentenceid = sentenceid;
        }
    }
}
