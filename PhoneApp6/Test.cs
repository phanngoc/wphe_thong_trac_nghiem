﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp6
{
    public class Test
    {
        public int id {set;get;}
        public String title{set;get;}
        public int cateid { set; get; }
        public String des { set; get; }
        public int countsen{set;get;}

        public int havedone { set; get; }
        public Test()
        {

        }
        public Test(int id,String title,int cateid,String des,int countsen,int havedone)
        {
            this.id = id;
            this.title = title;
            this.cateid = cateid;
            this.des = des;
            this.countsen = countsen;
            this.havedone = havedone;
        }

    }
}
