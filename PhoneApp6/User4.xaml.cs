﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
namespace PhoneApp6
{
    public partial class User4 : UserControl
    {
        public Test item1;
        public Test item2;

        public User4()
        {
            InitializeComponent();
        }
        public void setItem(ItemD itemd)
        {
            this.item1 = itemd.item1;
            this.item2 = itemd.item2;
            (A.Children[1] as TextBlock).Text = item1.title;
            
            if(item1.havedone==1)
            {
                (A.Children[0] as Image).Source = new BitmapImage(new Uri("Assets/Images/greenitem1c.jpg", UriKind.RelativeOrAbsolute));
            }
            
            
            if(item2 == null)
            {
                (B.Children[1] as TextBlock).Text = "";
                B.Children.RemoveAt(0);
            }
            else
            {
                (B.Children[1] as TextBlock).Text = item2.title;
                if(item2.havedone==1)
                {
                    (B.Children[0] as Image).Source = new BitmapImage(new Uri("Assets/Images/greenitem1c.jpg", UriKind.RelativeOrAbsolute));
                }
            }
            
        }

        private void A_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Helper.write("tap");
            if (item1 != null)
            {
                PhoneApplicationService.Current.State["item"] = item1;
                Uri uri = (new Uri("/PageChooseMethod.xaml", UriKind.RelativeOrAbsolute));
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri);
            }
        }

        private void B_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Helper.write("tap");
            if (item2 != null)
            {
                PhoneApplicationService.Current.State["item"] = item2;
                Uri uri = (new Uri("/PageChooseMethod.xaml", UriKind.RelativeOrAbsolute));
                (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(uri);
            }
        }
    }
}
