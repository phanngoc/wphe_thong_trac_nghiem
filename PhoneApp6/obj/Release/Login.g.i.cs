﻿#pragma checksum "D:\study\windowphone\PhoneApp6\PhoneApp6\Login.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2D424464E38DCAAB0AADC20791CFDD59"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace PhoneApp6 {
    
    
    public partial class Login : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.TextBox cusername;
        
        internal System.Windows.Controls.PasswordBox cpassword;
        
        internal System.Windows.Controls.PasswordBox ccpassword;
        
        internal System.Windows.Controls.Image showavatar;
        
        internal System.Windows.Controls.TextBox lusername;
        
        internal System.Windows.Controls.PasswordBox lpassword;
        
        internal System.Windows.Controls.Button Loginbutton;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/PhoneApp6;component/Login.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.cusername = ((System.Windows.Controls.TextBox)(this.FindName("cusername")));
            this.cpassword = ((System.Windows.Controls.PasswordBox)(this.FindName("cpassword")));
            this.ccpassword = ((System.Windows.Controls.PasswordBox)(this.FindName("ccpassword")));
            this.showavatar = ((System.Windows.Controls.Image)(this.FindName("showavatar")));
            this.lusername = ((System.Windows.Controls.TextBox)(this.FindName("lusername")));
            this.lpassword = ((System.Windows.Controls.PasswordBox)(this.FindName("lpassword")));
            this.Loginbutton = ((System.Windows.Controls.Button)(this.FindName("Loginbutton")));
        }
    }
}

