﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp6
{
    public class TopPoint
    {
        public float point {set;get;}
        public String name {set;get;}
        public String avatar {set;get;}
        public TopPoint(float point,String username,String avatar)
        {
            this.point = point;
            this.name = username;
            this.avatar = avatar;
        }
    }
}
