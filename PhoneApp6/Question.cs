﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp6
{
    public class Question
    {
        public int idquestion { set; get; }
        public String question { set; get; }
        public String answerA { set; get; }
        public String answerB { set; get; }
        public String answerC { set; get; }
        public String answerD { set; get; }
        public String correct { set; get; }

        public Test test { set; get; }

        public List<Comment> comments = new List<Comment>();


        public Question(int idquestion,String question, String answerA, String answerB, String answerC, String answerD, String correct, List<Comment> comments,Test test)
        {
            this.idquestion = idquestion;
            this.question = question;
            this.answerA = answerA;
            this.answerB = answerB;
            this.answerC = answerC;
            this.answerD = answerD;
            this.correct = correct;
            this.comments = comments;
            this.test = test;
        }
        public Question(int idquestion, String question,String correct, List<Comment> comments, Test test)
        {
            this.idquestion = idquestion;
            this.question = question;
            this.correct = correct;
            this.comments = comments;
            this.test = test;
        }
    }
}
