﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp6
{
    public class ItemD
    {
        public Test item1 { set; get; }
        public Test item2 { set; get; }
        public ItemD(Test item1,Test item2)
        {
            this.item1 = item1;
            this.item2 = item2;
        }
        public ItemD()
        {

        }
    }
}
