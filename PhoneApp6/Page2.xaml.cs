﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using ImageTools.IO;
using ImageTools.IO.Gif;
using ImageTools;
using ImageTools.Controls;
namespace PhoneApp6
{
    public partial class Page2 : PhoneApplicationPage
    {
        private int idparent;
        private bool flag_init = false;
        public Page2()
        {
            Sample smple = new Sample();
            smple.ImageSource = new Uri("Assets/Images/712.GIF", UriKind.Relative);
            this.DataContext = smple;

            //ImageLoading.Source = new ExtendedImage() { UriSource = new Uri("Assets/Images/301.GIF", UriKind.Relative) };
            ImageTools.IO.Decoders.AddDecoder<GifDecoder>();
            InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if(!flag_init)
            {
                flag_init = true;
                //idparent = (NavigationContext.QueryString["id"] as Category).id;
                var k = PhoneApplicationService.Current.State["id"];
                idparent = (k as Category).id;
                panorama.Title = (k as Category).name;
                //idparent = int.Parse(NavigationContext.QueryString["id"]);
                Helper.write("id parent la:" + idparent);
                init();
            }
        }
        public void init()
        {
            string avatarUri = "http://phanngoc.uni.me/tracnghiem/index.php/blog/getCateByIdParent/" + idparent;

            HttpWebRequest WebReq =
              (HttpWebRequest)WebRequest.Create(avatarUri);
            WebReq.Method = "GET";
            WebReq.BeginGetResponse(new AsyncCallback(getWord), WebReq);
        }
        private void getWord(IAsyncResult result)
        {
            HttpWebRequest webRequest = (HttpWebRequest)result.AsyncState;
            var httpResponse = (HttpWebResponse)webRequest.EndGetResponse(result);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string responseText = streamReader.ReadToEnd();
                processJson(responseText);
                streamReader.Close();
            }
        }
        private void processJson(string result)
        {
            result = Regex.Replace(result, "<!--.*?-->", "", RegexOptions.Singleline);
            result = Regex.Replace(result, "<script.*?script>", "", RegexOptions.Singleline);
            Helper.write("result:" + result);
            JArray jAr = JArray.Parse(result);
            List<Category> temp;
            temp = new List<Category>();
            for (int i = 0; i < jAr.Count; i++)
            {
                temp.Add(new Category((int)jAr[i]["ID"], (String)jAr[i]["NAME"], (int)jAr[i]["IDPARENT"]));
            }
            UIThread.Invoke(() => { createLayout(temp); ImageLoading.Visibility = Visibility.Collapsed; });
        }
        private void createLayout(List<Category> temp)
        {
            int count = temp.Count / 4;
            int excess = temp.Count % 4;
            if(excess != 0)
            {
                count ++;
            }
            int i=0;
            while (i < count)
            {
                
                ItemG item = new ItemG();

                if((i*4+3) < temp.Count)
                {
                    item = new ItemG(temp[i * 4], temp[i * 4 + 1], temp[i * 4 + 2], temp[i * 4 + 3]);
                }
                else if((i*4+2) < temp.Count)
                {
                    item = new ItemG(temp[i * 4], temp[i * 4 + 1], temp[i * 4 + 2], null);
                }
                else if ((i * 4 + 1) < temp.Count)
                {
                    item = new ItemG(temp[i * 4], temp[i * 4 + 1], null , null);
                }
                else if ((i * 4) < temp.Count)
                {
                    item = new ItemG(temp[i * 4], null , null , null);
                }
                Helper.write("add may lan");
                User3 user3 = new User3();
                user3.setItem(item);
                PanoramaItem panoramaCtrlItem = new PanoramaItem();
                //panoramaCtrlItem.Header = "Mục "+i;
                panoramaCtrlItem.Content = user3;
                panorama.Items.Add(panoramaCtrlItem);
                i++;
                
            }
        }
    }
    
}