﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp6
{
    public class ItemG
    {
        public Category item1 { set; get; }
        public Category item2 { set; get; }
        public Category item3 { set; get; }
        public Category item4 { set; get; }
        public ItemG(Category item1, Category item2, Category item3, Category item4)
        {
            this.item1 = item1;
            this.item2 = item2;
            this.item3 = item3;
            this.item4 = item4;
        }
        public ItemG()
        {

        }
    }
}
