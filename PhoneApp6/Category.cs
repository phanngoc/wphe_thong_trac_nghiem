﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp6
{
    public class Category
    {
        public int id { get; set; }
        public String name { get; set; }
        public int idParent { get; set; }
        public Category(int id,String name,int idParent)
        {
            this.id = id ;
            this.name = name;
            this.idParent = idParent;
        }
    }
}
