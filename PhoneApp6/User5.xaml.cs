﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using System.Windows;

namespace PhoneApp6
{
    public partial class User5 : UserControl
    {
        public User5()
        {
            InitializeComponent();
            // Change the background code using the same that the phone's theme (light or dark).
            this.panelSplashScreen.Background =
              new SolidColorBrush((Color)new PhoneApplicationPage().Resources["PhoneBackgroundColor"]);
            this.panelSplashScreen.Opacity = 0.7;
            // Adjust the code to the width of the actual screen
            this.progressBar1.Width = this.panelSplashScreen.Width =
              Application.Current.Host.Content.ActualWidth;
            this.progressBar1.Height = this.panelSplashScreen.Height = Application.Current.Host.Content.ActualHeight;
        }
    }
}
