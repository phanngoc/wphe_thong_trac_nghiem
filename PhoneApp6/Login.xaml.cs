﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text;
using System.IO;
using Microsoft.Phone.Tasks;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;
using System.Windows.Controls.Primitives;
using Microsoft.Phone.Net.NetworkInformation;

namespace PhoneApp6
{
    public partial class Login : PhoneApplicationPage
    {
        private MemoryStream photoStream = new MemoryStream();
        private string fileName;
        //flag = 0 // dang create account
        //flag = 1 // dang login
        int flag;
        User userc;
        Popup popup;

        public Login()
        {
            InitializeComponent();
            // Create a popup, where we'll show the ProgressBar
            popup = new Popup();
            // Create an object of type SplashScreen.
            User5 splash = new User5();
            popup.Child = splash;
            popup.IsOpen = false;
            LayoutRoot.Children.Add(popup);
        }
         protected override void OnNavigatedTo(NavigationEventArgs e)
         {
            base.OnNavigatedTo(e);
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            // txtInput is a TextBox defined in XAML.
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
            if (settings.Contains("usercurrent"))
            {
                NavigationService.Navigate(new Uri("/Page1.xaml", UriKind.RelativeOrAbsolute));
            }
         }
        

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            PhotoChooserTask task = new PhotoChooserTask();
            task.Completed += task_Completed;
            task.Show();
        }

        private void task_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult != TaskResult.OK)
                return;
            
            e.ChosenPhoto.CopyTo(photoStream);
            fileName = e.OriginalFileName;
           
            showavatar.Source = new BitmapImage(new Uri(e.OriginalFileName));

           // UploadImage(photoStream.ToArray(), "bom", "firstimage", null);
            

        }

        private string _lolThreading;

        private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
            // End the stream request operation
            Stream postStream = request.EndGetRequestStream(asynchronousResult);

            // avoid bad-touching UI stuff
            byte[] byteArray = Encoding.UTF8.GetBytes(_lolThreading);

            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();

            //Start the web request
            request.BeginGetResponse(new AsyncCallback(GetResponceStreamCallback), request);

        }
        void GetResponceStreamCallback(IAsyncResult callbackResult)
        {
            HttpWebRequest request = (HttpWebRequest)callbackResult.AsyncState;
            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(callbackResult);
            using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
            {
                UIThread.Invoke(() => {
                    popup.IsOpen = false;
                });
                string result = httpWebStreamReader.ReadToEnd();
                //For debug: show results
                result = Regex.Replace(result, "<!--.*?-->", "", RegexOptions.Singleline);
                result = Regex.Replace(result, "<script.*?script>", "", RegexOptions.Singleline);
                Helper.write(result);
                                    if(flag==1)
                                    {
                                        
                                        JArray jAr = JArray.Parse(result);
                                        if(jAr.Count==0)
                                        {
                                            UIThread.Invoke(() =>
                                            {
                                                MessageBox.Show("Username hoặc password không đúng");
                                            });  
                                        }
                                        else
                                        {
                                            User user = new User((int)jAr[0]["ID"], (String)jAr[0]["USERNAME"], (String)jAr[0]["PASSWORD"],(String) jAr[0]["USERNAME"],"http://phanngoc.uni.me/tracnghiem/public/data/avatar/"+(String) jAr[0]["AVATAR"]);
                                            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                                            // txtInput is a TextBox defined in XAML.
                                            if (!settings.Contains("usercurrent"))
                                            {
                                                settings.Add("usercurrent", user); 
                                                settings.Save();
                                            }
                                            UIThread.Invoke(() => {
                                                NavigationService.Navigate(new Uri("/Page1.xaml", UriKind.RelativeOrAbsolute));
                                            });
                                            
                                        }
                                     }
                
                                    else if(flag==0)
                                    { 
                                        
                                            JArray jAr = JArray.Parse(result);
                                            if (jAr.Count == 0)
                                            {
                                                UIThread.Invoke(() =>
                                                {
                                                    MessageBox.Show("Username đã tồn tại rồi");
                                                });
                                            }
                                            else
                                            {
                                                User user = new User((int)jAr[0]["ID"], (String)jAr[0]["USERNAME"], (String)jAr[0]["PASSWORD"], (String)jAr[0]["USERNAME"], "http://phanngoc.uni.me/tracnghiem/public/data/avatar/"+(String)jAr[0]["AVATAR"]);
                                                IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
                                                // txtInput is a TextBox defined in XAML.
                                                if (!settings.Contains("usercurrent"))
                                                {
                                                    settings.Add("usercurrent", user);
                                                    settings.Save();
                                                }
                                                UIThread.Invoke(() =>
                                                {
                                                    NavigationService.Navigate(new Uri("/Page1.xaml", UriKind.RelativeOrAbsolute));
                                                });
                                            }
                                    }
                }
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckNetworkConnection())
            {
                MessageBox.Show("Không có kết nối internet");
                return;
            }

            popup.IsOpen = true;
            flag = 1;
            // Create the post data
             _lolThreading = "username=" + lusername.Text + "&password=" + lpassword.Password;

            var request = HttpWebRequest.Create("http://phanngoc.uni.me/tracnghiem/index.php/blog/login") as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);
        }
        public static bool CheckNetworkConnection()
        {
            var networkInterface = NetworkInterface.NetworkInterfaceType;

            bool isConnected = false;
            if ((networkInterface == NetworkInterfaceType.Wireless80211) || (networkInterface == NetworkInterfaceType.MobileBroadbandGsm) || (networkInterface == NetworkInterfaceType.MobileBroadbandCdma))
                isConnected = true;

            else if (networkInterface == NetworkInterfaceType.None)
                isConnected = false;
            return isConnected;
        }

        private void Create_Account_Click(object sender, RoutedEventArgs e)
        {
            if(!CheckNetworkConnection())
            {
                MessageBox.Show("Không có kết nối internet");
                return;
            }
            popup.IsOpen = true;
            flag = 0;
            // Create the post data
            
            if(cpassword.Password!=ccpassword.Password)
            {
                MessageBox.Show("Bạn confirm password không đúng .Vui lòng nhập lại");
                return;
            }
        
            string base64string = Convert.ToBase64String(photoStream.ToArray());
            //Helper.write(base64string);
            _lolThreading = "username=" + cusername.Text + "&password=" + cpassword.Password + "&image=" + base64string;
            userc = new User(0, cusername.Text, cpassword.Password, cusername.Text, "");
            var request = HttpWebRequest.Create("http://phanngoc.uni.me/tracnghiem/index.php/blog/create") as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);
        }

    }
}