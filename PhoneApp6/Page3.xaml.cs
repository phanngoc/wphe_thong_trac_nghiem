﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.IO.IsolatedStorage;
using ImageTools.IO;
using ImageTools.IO.Gif;
using ImageTools;
using ImageTools.Controls;
namespace PhoneApp6
{
    public partial class Page3 : PhoneApplicationPage
    {
        private int idcate;
        bool flag_init = false;
        WebClient client;
        public Page3()
        {
            InitializeComponent();
            Sample smple = new Sample();
            smple.ImageSource = new Uri("Assets/Images/712.GIF", UriKind.Relative);
            this.DataContext = smple;

            //ImageLoading.Source = new ExtendedImage() { UriSource = new Uri("Assets/Images/301.GIF", UriKind.Relative) };
            ImageTools.IO.Decoders.AddDecoder<GifDecoder>();

            client = new WebClient();
            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(client_DownloadStringCompleted);

        }
        void client_DownloadStringCompleted(object sender,DownloadStringCompletedEventArgs e)
        {
            processJson(e.Result);
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if(!flag_init)
            {
                flag_init = true;
                var k = PhoneApplicationService.Current.State["item"];
                idcate = (k as Category).id;

                //idparent = int.Parse(NavigationContext.QueryString["id"]);
                Helper.write("id parent la:" + idcate);
                init();
            }
            //idparent = (NavigationContext.QueryString["id"] as Category).id;
          
        }
        public void init()
        {
             IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            // txtInput is a TextBox defined in XAML.
            User user;
            settings.TryGetValue<User>("usercurrent", out user);
            Random rnd = new Random();
            int randemitcache = rnd.Next(1, 1000); // creates a number between 1 and 12
            string avatarUri = "http://phanngoc.uni.me/tracnghiem/index.php/blog/category/" + idcate + "/" + user.id + "?random=" + randemitcache;
            client.DownloadStringAsync(new Uri(avatarUri));

            //HttpWebRequest WebReq =
            //  (HttpWebRequest)WebRequest.Create(avatarUri);
            //WebReq.Method = "GET";
            //WebReq.BeginGetResponse(new AsyncCallback(getWord), WebReq);
        }
        private void getWord(IAsyncResult result)
        {
            HttpWebRequest webRequest = (HttpWebRequest)result.AsyncState;
            var httpResponse = (HttpWebResponse)webRequest.EndGetResponse(result);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string responseText = streamReader.ReadToEnd();
                processJson(responseText);
                streamReader.Close();
            }
        }
        private void processJson(string result)
        {
            result = Regex.Replace(result, "<!--.*?-->", "", RegexOptions.Singleline);
            result = Regex.Replace(result, "<script.*?script>", "", RegexOptions.Singleline);
            Helper.write("result:" + result);
            JArray jAr = JArray.Parse(result);
            List<Test> temp;
            temp = new List<Test>();
            for (int i = 0; i < jAr.Count; i++)
            {
                temp.Add(new Test((int)jAr[i]["ID"], (String)jAr[i]["TITLE"], (int)jAr[i]["CATEID"], (String)jAr[i]["DES"], (int)jAr[i]["COUNTSEN"],(int)jAr[i]["HAVEDONE"]));
            }
            UIThread.Invoke(() => { createLayout(temp); ImageLoading.Visibility = Visibility.Collapsed; });
        }
        private void createLayout(List<Test> temp)
        {
            int count = temp.Count / 2;
            int excess = temp.Count % 2;
            if (excess != 0)
            {
                count++;
            }
            int i = 0;
            while (i < count)
            {

                ItemD item = new ItemD();

                if ((i * 2 + 1) < temp.Count)
                {
                    item = new ItemD(temp[i * 2], temp[i * 2 + 1]);
                }
                else
                {
                    item = new ItemD(temp[i * 2], null);
                }
                Helper.write("add may lan");
                User4 user4 = new User4();
                user4.setItem(item);
                listtest.Children.Add(user4);
                i++;
            }
        }
    }
}