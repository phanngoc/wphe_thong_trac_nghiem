﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using ImageTools.IO;
using ImageTools.IO.Gif;
using ImageTools;
using ImageTools.Controls;
using System.Windows.Controls.Primitives;

namespace PhoneApp6
{
    public partial class PageChooseMethod : PhoneApplicationPage
    {
        Popup popup;
        public PageChooseMethod()
        {
            InitializeComponent();


            //ImageTools.IO.Decoders.AddDecoder<GifDecoder>();
            popup = new Popup();
            // Create an object of type SplashScreen.
            User5 splash = new User5();
            popup.Child = splash;
            popup.IsOpen = true;
            LayoutRoot.Children.Add(popup);
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var k = PhoneApplicationService.Current.State["item"];
           
            int idtest = (k as Test).id;

          /*  IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            User user;
            settings.TryGetValue<User>("usercurrent", out user);
            Helper.write("" + idtest+"|"+user.id);
            string avatarUri = "http://phanngoc.uni.me/tracnghiem/index.php/blog/updateUserHaveDoneTest/" + idtest + "/" + user.id;
            HttpWebRequest WebReq =
              (HttpWebRequest)WebRequest.Create(avatarUri);
            WebReq.Method = "GET";
            WebReq.BeginGetResponse(getWord, WebReq);
            */

            WebClient client = new WebClient();
            client.DownloadStringCompleted += new DownloadStringCompletedEventHandler(client_DownloadStringCompleted);
            Random rnd = new Random();
            int randemitcache = rnd.Next(1, 1000); // creates a number between 1 and 12
            string urltoppoint = "http://phanngoc.uni.me/tracnghiem/index.php/blog/getPeopleAndMaxPointOfTest/" + idtest + "?random=" + randemitcache;
            client.DownloadStringAsync(new Uri(urltoppoint));
        }
        void client_DownloadStringCompleted(object sender,DownloadStringCompletedEventArgs e)
        {
            processJson(e.Result);
        }
        private void processJson(string result)
        {
            Helper.write("result:" + result);
            result = Regex.Replace(result, "<!--.*?-->", "", RegexOptions.Singleline);
            result = Regex.Replace(result, "<script.*?script>", "", RegexOptions.Singleline);
            JArray jAr = JArray.Parse(result);
            List<TopPoint> temp;
            temp = new List<TopPoint>();
            for (int i = 0; i < jAr.Count; i++)
            {
                //temp.Add(new Category((int)jAr[i]["ID"], (String)jAr[i]["NAME"], (int)jAr[i]["IDPARENT"]));
                temp.Add(new TopPoint((float)jAr[i]["POINT"], (String)jAr[i]["user"][0]["USERNAME"], "http://phanngoc.uni.me/tracnghiem/public/data/avatar/"+(String)jAr[i]["user"][0]["AVATAR"]));
            }
            UIThread.Invoke(() => {
                if (temp.Count == 0)
                {
                    listTopPoint.Visibility = Visibility.Collapsed;
                }
                else 
                {
                    listTopPoint.ItemsSource = temp;
                    nopeople.Visibility = Visibility.Collapsed;
                }
                    
                    popup.IsOpen = false;
                //ImageLoading.Visibility = Visibility.Collapsed; 
            });
        }
        private void getWord(IAsyncResult result)
        { 
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Uri uri = (new Uri("/DoTest/MainPage.xaml", UriKind.RelativeOrAbsolute));
            NavigationService.Navigate(uri); 
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Uri uri = (new Uri("/MainPage.xaml", UriKind.RelativeOrAbsolute));
            NavigationService.Navigate(uri); 
        }
    }
}