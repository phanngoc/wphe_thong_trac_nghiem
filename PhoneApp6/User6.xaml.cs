﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace PhoneApp6
{
    public partial class User6 : UserControl
    {
        public User6()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content as MainPage;
            currentPage.resetTest();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var currentPage = ((PhoneApplicationFrame)Application.Current.RootVisual).Content as MainPage;
            currentPage.backToTest();
        }
    }
}
