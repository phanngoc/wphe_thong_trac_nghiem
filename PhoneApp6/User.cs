﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Windows;
namespace PhoneApp6
{

    public class User
    {
        public int id { get; set; }
        public String username { set; get; }
        public String password { set; get; }
        public String fullname { set; get; }

        public String avatar { set; get; }

        public User()
        {

        }

        public User(int id,String username,String password,String fullname,String avatar)
        {
            this.id = id;  
            this.username = username;
            this.password = password;
            this.fullname = fullname;
            this.avatar = avatar;
        }
          
    }
}
