﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using ImageTools.IO;
using ImageTools.IO.Gif;
using ImageTools;
using ImageTools.Controls;
using System.IO.IsolatedStorage;
using System.Windows.Media.Imaging;
namespace PhoneApp6
{
    public partial class Page1 : PhoneApplicationPage
    {
        public Page1()
        {
            Sample smple = new Sample();
            smple.ImageSource = new Uri("Assets/Images/712.GIF", UriKind.Relative);
            this.DataContext = smple; 

            //ImageLoading.Source = new ExtendedImage() { UriSource = new Uri("Assets/Images/301.GIF", UriKind.Relative) };
            ImageTools.IO.Decoders.AddDecoder<GifDecoder>();

            InitializeComponent();
            if(!Login.CheckNetworkConnection())
            {
                MessageBox.Show("Không có kết nối internet");
            }
            else
            {
                init();
            }
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            // txtInput is a TextBox defined in XAML.
            User user = new User();
            if (settings.Contains("usercurrent"))
            {
                settings.TryGetValue<User>("usercurrent", out user);
            }
            avatar.Source = new BitmapImage(new Uri(user.avatar, UriKind.Absolute));
            username.Text = user.username;
        }
        public void init()
        {
            string avatarUri = "http://phanngoc.uni.me/tracnghiem/index.php/blog/getCateByIdParent/0";

            HttpWebRequest WebReq =
              (HttpWebRequest)WebRequest.Create(avatarUri);
            WebReq.Method = "GET";
            WebReq.BeginGetResponse(new AsyncCallback(getWord), WebReq);
        }
        private void getWord(IAsyncResult result)
        {
            HttpWebRequest webRequest = (HttpWebRequest)result.AsyncState;
            var httpResponse = (HttpWebResponse)webRequest.EndGetResponse(result);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string responseText = streamReader.ReadToEnd();
                processJson(responseText);
                streamReader.Close();
            }
        }
        private void processJson(string result)
        {

            result = Regex.Replace(result, "<!--.*?-->", "", RegexOptions.Singleline);
            result = Regex.Replace(result, "<script.*?script>", "", RegexOptions.Singleline);
            Helper.write("result:" + result);
            JArray jAr = JArray.Parse(result);
            List<Category> temp;
            temp = new List<Category>();
            for (int i = 0; i < jAr.Count; i++)
            {
                temp.Add(new Category((int)jAr[i]["ID"], (String)jAr[i]["NAME"], (int)jAr[i]["IDPARENT"]));
            }
            UIThread.Invoke(() => { listcate.ItemsSource = temp; ImageLoading.Visibility = Visibility.Collapsed; });
        }

        private void listcate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Helper.write("vao:" + (listcate.SelectedItem as Category).id);
            PhoneApplicationService.Current.State["id"] = (listcate.SelectedItem as Category);

            NavigationService.Navigate(new Uri("/Page2.xaml", UriKind.RelativeOrAbsolute));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            // txtInput is a TextBox defined in XAML.
           // User user;
            if (settings.Contains("usercurrent"))
            {
                //settings.TryGetValue<User>("usercurrent", out user);
                settings.Remove("usercurrent");
            }
            while (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
           // NavigationService.RemoveBackEntry();
            NavigationService.Navigate(new Uri("/Login.xaml", UriKind.RelativeOrAbsolute));
            //NavigationService.RemoveBackEntry();
            //NavigationService.GoBack();
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            // do some stuff ...
            Application.Current.Terminate();
         
        }
    }
}