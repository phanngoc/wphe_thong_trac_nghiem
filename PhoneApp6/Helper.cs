﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.CompilerServices;
namespace PhoneApp6
{
    public class Helper
    {

        public static Helper instance = null;
        public static List<Trace> collectTrace = new List<Trace>();
        public static Helper getInstance()
        {
            if (instance != null)
            {
                return instance;
            }
            else
            {
                instance = new Helper();
                return instance;
            }
        }
        public Helper()
        {

        }
        public static void report()
        {

        }
 
        public static void write(String text,[CallerLineNumber] int sourceLineNumber = 0)
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);
            System.Diagnostics.Debug.WriteLine(sf.GetMethod().DeclaringType + " @ " + sf.GetMethod().Name + " @ " + sourceLineNumber + " : " + text);
     
        }
        public void writeOnly(String text)
        {
            
        }
        public bool checkExit(Trace trace)
        {
            foreach(Trace tra in collectTrace)
            {
                if (tra.equals(trace))
                {
                    return true;
                }
            }
            return false;
        }
    }
    public class Trace
    {
        public int numberline { set; get; }
        public bool isPassed { set; get; }

        public bool isWrite { set; get; }

        public String description { set; get; }
        public String method { set; get; }
        public String classname { set; get; }
        public Trace(int numberline, String des, String method, String classname)
        {
            this.numberline = numberline;
            this.description = des;
            this.method = method;
            this.classname = classname;
            isWrite = true;
        }
        public bool equals(Trace obj)
        {
            return (numberline == obj.numberline) && (classname == obj.classname) && (method == obj.method);
        }
        public void changeAccess()
        {
            isWrite = !isWrite;
        }
    }
}
